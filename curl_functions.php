<?php

function get_api_url()
{
    return 'http://localhost:8080/v1/api';
}

function send_request_move($boardStatus)
{
    $url = get_api_url() . "/move";
    $boardStatus = [
        $boardStatus[0],
        $boardStatus[1],
        $boardStatus[2],
    ];
    $jsonData = [
        'player' => 'X',
        'currentBoardState' => $boardStatus
    ];

    $jsonData = json_encode($jsonData);

    $ch = curl_init($url);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
    curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonData);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
            'Content-Length: ' . strlen($jsonData))
    );

    $resultUser = curl_exec($ch);
    $resultUser = json_decode($resultUser);
    curl_close($ch);

    $resultBot = send_request_move_bot($resultUser->boardState);
    return $resultBot;
}

function send_request_move_bot($boardStatus)
{
    $url = get_api_url();
    $url .= "/move-bot";

    $boardStatus = [
        $boardStatus[0],
        $boardStatus[1],
        $boardStatus[2],
    ];
    $jsonData = [
        'player' => 'O',
        'currentBoardState' => $boardStatus
    ];

    $jsonData = json_encode($jsonData);

    $ch = curl_init($url);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
    curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonData);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
            'Content-Length: ' . strlen($jsonData))
    );

    $result = curl_exec($ch);
    curl_close($ch);
    return $result;
}

function prepare_board_state()
{
    $boardState = [];
    foreach ($_POST as $fieldName => $param) {
        $fieldData = str_replace("x_", "", $fieldName);
        $fieldData = str_replace("y_", "", $fieldData);
        $fieldData = explode("_", $fieldData);
        $cell = $fieldData[1];
        $row  = $fieldData[0];
        if (is_numeric($row) && is_numeric($cell)) {
            $boardState[$row][$cell] = strtoupper($param);
        }
    }

    return $boardState;
}
